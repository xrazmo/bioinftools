import datetime
import os
import sqlite3 as lit
from glob import glob
from multiprocessing.pool import Pool
from subprocess import Popen
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

from lib.Utility import bcolors

__author__ = 'xrazmo'


class ProdigalOption:
    def __init__(self, inputDir, outDir, dbPath='',
                 extension='*.fasta',
                 instanceNum=1,
                 saveinDB=False,
                 out_prot=False,
                 out_nucl=False,
                 resetTbl=False,
                 saveGene=False,
                 saveProtein=False,
                 prodigalcmd='prodigal'):
        self.prodigalcmd = prodigalcmd
        self.dbPath = dbPath
        self.inputDir = inputDir
        self.re_extension = extension
        self.outDir = outDir
        self.resetTbl = resetTbl
        self.threadNum = instanceNum
        self.out_prot = out_prot
        self.out_nucl = out_nucl
        self.saveGene = saveGene
        self.saveProtein = saveProtein
        self.saveinDB = saveinDB


class ProdigalResult:
    def __init__(self):
        self.tag = ''
        self.no = ''
        self.startIdx = 0
        self.endIdx = 0
        self.strand = ''
        self.partial = ''
        self.startType = ''
        self.rbsMotif = ''
        self.rbsSpacer = ''
        self.gccontent = 0
        self.conf = 0
        self.csscore = 0
        self.ssscore = 0
        self.rscore = 0
        self.uscore = 0
        self.tscore = 0
        self.proseq = ''
        self.dnaseq = ''


def runProdigal(option):
    print(f"\n{bcolors.CVIOLET2}----Running Prodigal----{bcolors.CEND}")
    assert isinstance(option, ProdigalOption)
    try:
        dt = datetime.datetime.now()
        option.outDir = os.path.join(option.outDir, f"prodigalResult_{dt.strftime('%Y_%m_%d_%H-%M')}")
        os.mkdir(option.outDir)
        os.chdir(option.outDir)
        print(f"{bcolors.CURL}{option.outDir}{bcolors.CEND}")
    except Exception as e:
        print(f"{bcolors.CYELLOW2}{e}{bcolors.CEND}")

    fileNames = []
    for dir in option.inputDir:
        fileNames += glob(os.path.join(dir, option.re_extension))

    print(f'\n\t{bcolors.CYELLOW}{len(fileNames)} input files were detected.{bcolors.CEND}\n')

    cmdList = []
    for ff in fileNames:
        fname = os.path.basename(ff).split('.')[0]

        outCoordF = os.path.join(option.outDir, f'{fname}_coords.txt')

        tcommand = f"{option.prodigalcmd} -i {ff} -o {outCoordF} "

        if option.out_prot:
            proteinF = os.path.join(option.outDir, f'{fname}_proseq.faa')
            tcommand = tcommand + f" -a {proteinF}"
        if option.out_nucl:
            dnaF = os.path.join(option.outDir, f'{fname}_dnaSeq.fna')
            tcommand = tcommand + f" -d {dnaF}"

        tcommand = tcommand + " -f gff -q -m"
        cmdList.append("nice -1 " + tcommand)

    print('\n\tRunnig the following commands:')
    processes = set()
    for cmd in cmdList:
        print(f"\n\t{bcolors.CBLUE2}{cmd}{bcolors.CEND}")
        processes.add(Popen(cmd, shell=True))
        if len(processes) >= option.threadNum:
            os.wait()
            processes.difference_update([p for p in processes if p.poll() is not None])

    for p in processes:
        if p.poll() is None:
            p.wait()

    coordfiles = glob(os.path.join(option.outDir, "*_coords.txt"))
    faafiles = glob(os.path.join(option.outDir, "*_proseq.faa"))
    fnafiles = glob(os.path.join(option.outDir, "*_dnaSeq.fna"))

    print(
        f'\n\tWe could detect {bcolors.CBLUE2} {len(coordfiles)} coord files {bcolors.CEND}, {bcolors.CGREEN2} {len(fnafiles)} fna files (genes){bcolors.CEND},'
        f' {bcolors.CYELLOW2} {len(faafiles)} faa files (proteins){bcolors.CEND} were detected.\n')
    if len(coordfiles) == len(fileNames):
        print(f"{bcolors.CGREEN}Prodigal was performed successfully!{bcolors.CEND}")
    else:
        print(
            f"{bcolors.CYELLOW2}\tPotential missing of information in the downstream analyses: 'Coord file is missing!'. \nCheck the results manually.{bcolors.CEND}")

    return option.outDir


def saveResult(dbPath, coordDir, saveGene, saveProtein):
    print(f'\n{bcolors.CVIOLET2}-----Saving ORFs-----{bcolors.CEND}')
    print(f"\t{bcolors.CURL}{os.path.abspath(dbPath)}{bcolors.CEND}\n")

    coordFiles = glob(os.path.join(coordDir, '*coords*'))
    conn = lit.connect(dbPath)
    cur = conn.cursor()
    print(f'\tReading coord files:')
    for cfn in coordFiles:
        print(f"\t\t{bcolors.CURL}{bcolors.CBLUE}{cfn}{bcolors.CEND}\n")
        fid = os.path.basename(cfn).split('_coords')[0]
        geneFasta = (None, os.path.join(coordDir, '{}_dnaSeq.fna'.format(fid)))[saveGene]
        ORFFasta = (None, os.path.join(coordDir, '{}_proSeq.faa'.format(fid)))[saveProtein]

        if (not parseCoordFiles(cfn, cur, geneFasta, ORFFasta)):
            return False
    conn.commit()
    conn.close()
    print(f"{bcolors.CGREEN2}\tORFs were saved in the reference database successfully!{bcolors.CEND}")
    return True

def parseCoordFiles(coordFile, cursor, geneFasta, proFasta):
    try:
        if geneFasta is not None:
            geneDic = SeqIO.index(geneFasta, 'fasta')
        if proFasta is not None:
            proDic = SeqIO.index(proFasta, 'fasta')
    except Exception as e:
        print(f"{bcolors.CRED2}{e}{bcolors.CEND}")
        return False

    contigsAcc = os.path.basename(coordFile).split('_coords.txt')[0]

    with open(coordFile) as hdl:
        ccount = 1
        pror = ProdigalResult()

        for ll in hdl:
            if ll[0] == '#':  # First #-line
                ccount = 1
                continue
            else:
                lineList = ll.split(';')
                tstr = lineList[0].split()
                pror.tag = tstr[0]
                pror.no = ccount
                ccount += 1
                pror.startIdx = int(tstr[3])
                pror.endIdx = int(tstr[4])
                pror.strand = ('F', 'R')[tstr[6] == '-']
                pror.partial = lineList[1].split('=')[1]
                pror.startType = lineList[2].split('=')[1]
                pror.rbsMotif = lineList[3].split('=')[1]
                pror.rbsSpacer = lineList[4].split('=')[1]
                pror.gccontent = float(lineList[5].split('=')[1])
                pror.conf = float(lineList[6].split('=')[1])
                pror.score = float(lineList[7].split('=')[1])
                pror.csscore = float(lineList[8].split('=')[1])
                pror.ssscore = float(lineList[9].split('=')[1])
                pror.rscore = float(lineList[10].split('=')[1])
                pror.uscore = float(lineList[11].split('=')[1])
                pror.tscore = float(lineList[12].split('=')[1])
                if proFasta is not None:
                    pror.proseq = "'{}'".format(str(proDic[pror.tag + "_" + str(pror.no)].seq))
                else:
                    pror.proseq = None
                if geneFasta is not None:
                    pror.dnaseq = "'{}'".format(str(geneDic[pror.tag + "_" + str(pror.no)].seq))
                else:
                    pror.dnaseq = None
                try:
                    cursor.execute("insert into tbl_prodigal(orfAcc,contigAcc,startIdx,endIdx,strand,startType,"
                                   "rbsMotif,rbsspacer,gccontent,conf,score,csScore,ssScore,rsScore,"
                                   "uscore,tscore,dnaSeq,proSeq) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                                   , (
                                   pror.tag + "_" + str(pror.no), contigsAcc, pror.startIdx, pror.endIdx, pror.strand,
                                   pror.startType, pror.rbsMotif, pror.rbsSpacer,
                                   pror.gccontent, pror.conf, pror.score, pror.csscore, pror.ssscore, pror.rscore,
                                   pror.uscore, pror.tscore, pror.dnaseq, pror.proseq))
                except Exception as e:
                    print(f"{bcolors.CRED2}{e}{bcolors.CEND}")
                    return False
    return True


def renameProdigalGenes(refDB, prodigalDir, outDir, threads=1):
    print(f'\n{bcolors.CVIOLET2}-----Renaming ORFs-----{bcolors.CEND}')
    conn = lit.connect(refDB)
    cur = conn.cursor()
    print('\tMaking accessions dictionary...')
    cmd = "select contigAcc,orfAcc,id from tbl_prodigal"
    acc2IdDic = {f"{rr[0]}@{rr[1]}": rr[2] for rr in cur.execute(cmd)}
    conn.close()

    fnaFiles = glob(os.path.join(prodigalDir, '*.fna'))
    cmdLst = []
    for fna in fnaFiles:
        cmdLst.append((fna, outDir, acc2IdDic))

    pool = Pool(processes=threads)
    pool.map(changeFastaIDs, cmdLst)
    print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')

def changeFastaIDs(param):
    ff, outd, accdic = param
    outF = os.path.join(outd, os.path.basename(ff))
    outF_hdl = open(outF, 'w+')
    print(f"\t{bcolors.CBLUE}{outF}...started.{bcolors.CEND}")
    fname = os.path.basename(ff).split('_dnaSeq')[0]
    with open(ff) as hdl:
        recLst = []
        for rec in SeqIO.parse(hdl, 'fasta'):
            newID = accdic[f"{fname}@{rec.id}"]
            recLst.append(SeqRecord(rec.seq, id=str(newID), description="", name=""))
            if len(recLst) > 1e4:
                SeqIO.write(recLst, outF_hdl, 'fna')
                recLst = []
        SeqIO.write(recLst, outF_hdl, 'fasta')
        print(f"\t{bcolors.CGREEN}{outF}...ready!{bcolors.CEND}")
