import os
from datetime import datetime
from glob import glob
from subprocess import Popen
from time import time
from Bio import SeqIO
from lib.Utility import bcolors
import sqlite3 as lit
from lib import Utility as ut
import pandas as pd


def downloadDB(outDir):
    print(f"\n{bcolors.CVIOLET2}----Downloading plasmidFinder database----{bcolors.CEND}")
    os.chdir(outDir)
    Popen("git clone https://bitbucket.org/genomicepidemiology/plasmidfinder_db.git", shell=True).wait()
    faFile = os.path.join(outDir, 'plasmidfinder_db/enterobacteriaceae.fsa')
    if os.path.exists(faFile):
        print(f"\t{bcolors.CBLUE}{bcolors.CURL}{faFile}{bcolors.CEND}")
        print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')
        return True
    else:
        print(f"\t{bcolors.CRED2}Error: could not locate 'enterobacteriaceae.fsa' file.{bcolors.CEND}")
        return False


def createBlastDB(fastaFile, outDir):
    print(f"\n{bcolors.CVIOLET2}----Creating Blastn database----{bcolors.CEND}")
    plfidb = os.path.join(outDir, 'plfi_db')
    cmd = f"makeblastdb -in {fastaFile} -input_type fasta -dbtype nucl -title plfi_db -out {plfidb}"
    print(f"\t{bcolors.CBLUE}{cmd}{bcolors.CEND}")
    Popen(cmd, shell=True).wait()
    print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')
    return plfidb


def ncbiBlastn_parallell(inDirs, outDir, blastdbpath, extension='fna', task='blastn', threadNum=32, instanceNum=5,
                         stringOption=''):
    print(f"\n{bcolors.CVIOLET2}----Running Blastn----{bcolors.CEND}")
    dt = datetime.now()
    outDir = os.path.join(outDir, f"blastn{dt.strftime('%Y_%m_%d_%H-%M')}")
    ut.makeDirectory(outDir)
    os.chdir(outDir)

    fetchedFiles = []
    for inDir in inDirs:
        fetchedFiles += glob(os.path.join(inDir, '*.{}'.format(extension)))

    print(f'\n\t{bcolors.CYELLOW}{len(fetchedFiles)} input files were detected.{bcolors.CEND}\n')

    outfrmt = '6'
    outFileLst = []
    cmdList = []
    for qry in fetchedFiles:
        outFname = os.path.basename(qry).split(f'.{extension}')[0]
        outF = os.path.join(outDir, '{}.b6'.format(outFname))
        cmd = f"nice -1 blastn -task {task} -query {qry} -db {blastdbpath} -out {outF} " \
              f"-num_threads {threadNum} -strand \'both\' -max_target_seqs 5 -outfmt {outfrmt} {stringOption}"
        cmdList.append(cmd)
        outFileLst.append(outF)

    print('\n\tRunnig the following commands:')
    processes = set()
    for cmd in cmdList:
        print(f"\n\t{bcolors.CBLUE}{cmd}{bcolors.CEND}")
        processes.add(Popen(cmd, shell=True))
        if len(processes) >= instanceNum:
            # print('Waiting...')
            os.wait()
            # print('Waked!')
            processes.difference_update([p for p in processes if p.poll() is not None])

    for p in processes:
        if p.poll() is None:
            p.wait()

    return outDir


def saveBlastn(dbpath, blastOutDir):
    print(f"\n{bcolors.CVIOLET2}----Saving results into database----{bcolors.CEND}")
    conn = lit.connect(dbpath)
    cur = conn.cursor()
    blastOuts = glob(os.path.join(blastOutDir, "*.b6"))
    for blastOutF in blastOuts:
        print(f"\t{bcolors.CBLUE2}{bcolors.CURL}{blastOutF}{bcolors.CEND}")
        with open(blastOutF) as hdl:
            sampleAcc = os.path.basename(blastOutF).split('.b6')[0]
            for ll in hdl:
                pp = ll.strip('\n').split()
                if len(pp) < 12:
                    print(f"{bcolors.CRED2}\tError:Reading a line with missing columns:{bcolors.CEND}")
                    print(f"\t{bcolors.CURL}{pp}{bcolors.CEND}")
                    continue

                # if float(pp[2]) < 90 or int(pp[0]) == int(pp[1]):
                #     continue
                try:
                    cur.execute(
                        "INSERT INTO tbl_blastn(sampleAcc,seqAcc,refAcc,pident,length,mismatch,gaps,qstart,qend,sstart,send,evalue,bitscore)" \
                        " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", (sampleAcc, pp[0], pp[1], pp[2], pp[3], pp[4], pp[5],
                                                                pp[6], pp[7], pp[8], pp[9], pp[10], pp[11]))
                except Exception as e:
                    print(f"{bcolors.CRED2}{e}{bcolors.CEND}")
    conn.commit()
    conn.close()

    print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')


def updateCov(db, refFasta):
    print(f"\n{bcolors.CVIOLET2}----Updating coverage----{bcolors.CEND}")
    seqDic = {}
    prog = 0
    tic = time()
    print('\tReading reference sequence:')
    with open(refFasta) as hdl:
        for rec in SeqIO.parse(hdl, 'fasta'):
            prog += 1
            toc = time()
            if toc - tic > 1:
                print(f"\r{prog}", end='')
                tic = time()
            seqDic[rec.id] = len(rec.seq)
    print(f"\r{prog}", end='')
    print('\n\tFetching records from database...')
    con = lit.connect(db)
    cur = con.cursor()

    cmd = "select id, refAcc ,abs(sstart-send)+1 from tbl_blastn where coverage is null"
    print('\tUpdating records...', end='')
    prog = 0
    rows = cur.execute(cmd).fetchall()
    for rr in rows:
        prog += 1
        toc = time()
        if toc - tic > 1:
            print(f"\rRecored {prog}", end="")
            tic = time()
        try:
            seqLen = seqDic[rr[1]]
            cov = round((float(rr[2]) / float(seqLen)) * 100.0, 2)
            cur.execute("update tbl_blastn set coverage=? where id = ?", (cov, rr[0]))
        except KeyError:
            print(f'\n\t{bcolors.CRED}Could not find {rr[1]} in reference fasta file.{bcolors.CEND}')

    con.commit()
    con.close()
    print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')


def exportResults(dbPath, outDir, identity=95, coverage=60, sep=";"):
    print(f"\n{bcolors.CVIOLET2}----Export results of plasmidFinder pipeline----{bcolors.CEND}")
    print(f"{bcolors.CYELLOW}Thresholds: identity>= {identity}% and coverage>= {coverage}%{bcolors.CEND}")

    conn = lit.connect(dbPath)
    cmd = f"SELECT Id,sampleAcc as sample,seqAcc as sequenceID,refAcc as plasmidMarker," \
          f"pident as identity,coverage, length," \
          f"evalue,bitscore,mismatch,gaps,qstart,qend,sstart,send" \
          f" FROM tbl_blastn where pident>={identity } and coverage >= {coverage} order by sample,identity,coverage DESC;"

    df = pd.read_sql_query(cmd, conn, index_col="Id")
    outF = os.path.join(outDir, f'PlamidFinder_results_{identity}_{coverage}.csv')
    df.to_csv(outF, sep=sep)
    conn.close()
    print(f'{bcolors.CGREEN}Check the results in: {bcolors.CURL}{outF}{bcolors.CEND}')
