import os
import yaml
import sqlite3 as lit


class bcolors:
    CEND = '\33[0m'
    CBOLD = '\33[1m'
    CITALIC = '\33[3m'
    CURL = '\33[4m'
    CBLINK = '\33[5m'
    CBLINK2 = '\33[6m'
    CSELECTED = '\33[7m'

    CBLACK = '\33[30m'
    CRED = '\33[31m'
    CGREEN = '\33[32m'
    CYELLOW = '\33[33m'
    CBLUE = '\33[34m'
    CVIOLET = '\33[35m'
    CBEIGE = '\33[36m'
    CWHITE = '\33[37m'

    CBLACKBG = '\33[40m'
    CREDBG = '\33[41m'
    CGREENBG = '\33[42m'
    CYELLOWBG = '\33[43m'
    CBLUEBG = '\33[44m'
    CVIOLETBG = '\33[45m'
    CBEIGEBG = '\33[46m'
    CWHITEBG = '\33[47m'

    CGREY = '\33[90m'
    CRED2 = '\33[91m'
    CGREEN2 = '\33[92m'
    CYELLOW2 = '\33[93m'
    CBLUE2 = '\33[94m'
    CVIOLET2 = '\33[95m'
    CBEIGE2 = '\33[96m'
    CWHITE2 = '\33[97m'

    CGREYBG = '\33[100m'
    CREDBG2 = '\33[101m'
    CGREENBG2 = '\33[102m'
    CYELLOWBG2 = '\33[103m'
    CBLUEBG2 = '\33[104m'
    CVIOLETBG2 = '\33[105m'
    CBEIGEBG2 = '\33[106m'
    CWHITEBG2 = '\33[107m'


def readConfFile(conffile, show=False):
    print(f'{bcolors.CVIOLET2}----input parameters----{bcolors.CEND}')
    print(f"{bcolors.CURL}{os.path.abspath(conffile)}{bcolors.CEND}")
    with open(conffile, "r") as ymlfile:
        cfg = yaml.load(ymlfile)
    if show:
        for sc in cfg:
            print(f"\t{bcolors.CBLUE2}- {sc}: {cfg[sc]} {bcolors.CEND}")

    return cfg


def createRefDB(outDir):
    cmdList = ["CREATE TABLE tbl_prodigal ("
               "    Id        INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
               "    orfAcc     VARCHAR ,"
               "    contigAcc     VARCHAR ,"
               "    startIdx  INTEGER,"
               "    endIdx    INTEGER,"
               "    strand    CHAR,"
               "    conf      FLOAT,"
               "    score     FLOAT,"
               "    rbsspacer FLOAT,"
               "    gccontent FLOAT,"
               "    startType VARCHAR,"
               "    rbsMotif  VARCHAR,"
               "    csScore   FLOAT,"
               "    ssScore   FLOAT,"
               "    rsScore   FLOAT,"
               "    uscore    FLOAT,"
               "    tscore    FLOAT,"
               "    clustNum  INTEGER,"
               "    tag       VARCHAR,"
               "    dnaSeq   VARCHAR,"
               "    proSeq   VARCHAR);",
               "CREATE TABLE tbl_blastxDiamond ("
               "    Id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
               "    orfAcc       VARCHAR,"
               "    refId       VARCHAR,"
               "    identity    FLOAT,"
               "    qcoverage   FLOAT,"
               "    scoverage   FLOAT,"
               "    length      INTEGER,"
               "    qlen      INTEGER,"
               "    slen      INTEGER,"
               "    mismatch    INTEGER,"
               "    gap         INTEGER,"
               "    qstart      INTEGER,"
               "    qend        INTEGER,"
               "    sstart      INTEGER,"
               "    send        INTEGER,"
               "    eValue      VARCHAR,"
               "    bitScore    FLOAT,"
               "    dbName      VARCHAR,"
               "    description VARCHAR,"
               "    tag         VARCHAR);",
               "CREATE TABLE tbl_blastn (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
               "sampleAcc    VARCHAR,"
               "seqAcc    VARCHAR, "
               "refAcc       VARCHAR,"
               "pident   FLOAT,"
               "coverage    FLOAT,"
               "length   INTEGER,"
               "evalue   VARCHAR,"
               "bitscore FLOAT,"
               "mismatch INTEGER,"
               "gaps     INTEGER,"
               "qstart   INTEGER,"
               "qend     INTEGER,"
               "sstart   INTEGER,"
               "send     INTEGER,"
               "tag      VARCHAR);"]
    dbpath = os.path.join(outDir, 'refDB.db')
    conn = lit.connect(dbpath)
    for cmd in cmdList:
        try:
            conn.execute(cmd)
        except Exception as e:
            print(e)

    return dbpath


def makeDirectory(indir):
    try:
        os.mkdir(indir)
        print(f'{bcolors.CGREEN}\tDirectory was made:{indir}{bcolors.CEND}')
    except Exception as e:
        print(f"{bcolors.CYELLOW}{e}{bcolors.CEND}")
    return indir

def readStatus(tmpdir):
    stF = os.path.join(tmpdir, 'status')
    if os.path.exists(stF):
        with open(stF) as hdl:
            ll = hdl.readline().strip('\r\n')
            return int(ll)
    else:
        print(
            f"{bcolors.CYELLOW2}Warning: could not retrieve status. "
            f"The pipeline is started in a new Directory again.{bcolors.CEND}")
        return 0


def writeStatus(tmpdir, status):
    stF = os.path.join(tmpdir, 'status')
    with open(stF, 'w') as hdl:
        hdl.write(str(status))
