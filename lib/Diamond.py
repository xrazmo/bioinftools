import math
from datetime import datetime
import os
import subprocess as sub
from glob import glob
import sqlite3 as lit
import pandas as pd
from lib.Utility import bcolors
from lib import Utility as ut

def runDiamond_parallel(paramDic):
    print(f"\n{bcolors.CVIOLET2}----Running Diamond----{bcolors.CEND}")

    dt = datetime.now()
    paramDic["outDir"] = ut.makeDirectory(os.path.join(paramDic["curDir"], f"diamond{dt.strftime('%Y_%m_%d_%H-%M')}"))
    os.chdir(paramDic["outDir"])

    fetchedFiles = []
    for indir in paramDic["inputDir"]:
        for ext in paramDic["valid_extensions"]:
            fetchedFiles += sorted(glob(os.path.join(indir, f"*.{ext}")))

    print(f'\n\t{bcolors.CYELLOW}{len(fetchedFiles)} input files were detected.{bcolors.CEND}\n')

    for dbname, db, option in paramDic["dbs"]:
        cmdList = []
        for ff in fetchedFiles:
            fbase = os.path.basename(ff)

            filename = fbase.split('.')[0]
            outb6 = os.path.join(paramDic["outDir"], f"{dbname}-{filename}.b6")
            if paramDic["task"].startswith('blastx'):
                cmdList.append(
                    f'nice -5 {paramDic["commandPath"]} blastx -p {paramDic["threads"]} -o {outb6} {option} -d {db} -q {ff} {paramDic["outCols"]} ')
            else:
                cmdList.append(
                    f'nice -5 {paramDic["commandPath"]} blastp -p {paramDic["threads"]} -o {outb6} {option} -d {db} -q {ff} {paramDic["outCols"]}')

            if len(cmdList) >= paramDic["batchSize"]:
                os_parallel(cmdList, paramDic["instance"], printCommand=True)
                cmdList = []

        os_parallel(cmdList, paramDic["instance"], printCommand=True)
    return paramDic["outDir"]


def os_parallel(commandLst, instanceNumber, printCommand=False):
    print('\n\tRunnig the following commands:')
    processes = set()
    for cmd in commandLst:
        if printCommand:
            print(f"\n\t{bcolors.CBLUE2}{cmd}{bcolors.CEND}")
        processes.add(sub.Popen(cmd, shell=True))
        if len(processes) >= instanceNumber:
            os.wait()
            processes.difference_update([p for p in processes if p.poll() is not None])

    for p in processes:
        if p.poll() is None:
            p.wait()


def saveResultsDirectory(refDB, b6Dir, patterns, paramDic):
    print(f"\n{bcolors.CVIOLET2}----Saving results into the reference database----{bcolors.CEND}")

    conn = lit.connect(refDB)
    cur = conn.cursor()
    logfile = os.path.join(os.path.dirname(refDB), "logERROR_insertDiamond.txt")
    print(f"\n\t{bcolors.CYELLOW}Check possible errors in: {bcolors.CURL}{logfile}{bcolors.CEND}{bcolors.CEND}\n")

    hdl_log = open(logfile, 'w+')
    collist = paramDic["outCols"].replace('-f 6','').strip(' \r\n')
    colsDic = {col.lower(): i for i, col in enumerate(collist.split())}

    b6Files = []
    for pt in patterns:
        b6Files += sorted(glob(os.path.join(b6Dir, pt)))

    for ff in b6Files:
        i = 0
        dbname = os.path.basename(ff).split('-')[0]
        errorCount = 0
        with open(ff) as hdl:
            print(f"\t{bcolors.CBLUE2}{ff}{bcolors.CEND}")
            for ll in hdl:
                i += 1
                cols = ll.strip('\n\r').split('\t')
                if len(cols) < 12:
                    continue
                GiveVal = lambda name: cols[colsDic[name]]
                alignmentLength = float(GiveVal("length"))
                qlen = float(GiveVal("qlen")) / (1., 3.)[paramDic["task"] == 'blastx']
                qcov = round(100.0 * alignmentLength / qlen)
                scov = round(100.0 * alignmentLength/ float(GiveVal("slen")))
                stitle = GiveVal("stitle").replace(GiveVal("sseqid"),'').strip(' ')
                try:
                    cur.execute(
                        "INSERT INTO tbl_blastxDiamond (orfAcc,refId,identity,qcoverage,scoverage,length,qlen,slen,mismatch,gap,"
                        "qstart,qend,sstart,send,eValue,bitScore,dbName,description) VALUES"
                        " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        (GiveVal("qseqid"), GiveVal("sseqid"), GiveVal("pident"), qcov, scov, GiveVal("length")
                         ,GiveVal("qlen"),GiveVal("slen"),GiveVal("mismatch"), GiveVal("gaps"),
                         GiveVal("qstart"), GiveVal("qend"), GiveVal("sstart"), GiveVal("send"),
                         GiveVal("evalue"), GiveVal("bitscore"), dbname,stitle))
                except Exception as e:
                    errorCount += 1
                    hdl_log.write(f"{errorCount}- Error in line number {i} of {os.path.basename(ff)} "
                                  f"\n\toriginal line:{ll}"
                                  f"\n\tException:'{e}'")
        if errorCount>0:
            print(f"\t{bcolors.CRED}{errorCount} detected while processing {bcolors.CURL}{ff}{bcolors.CEND}{bcolors.CEND}")

        conn.commit()
        # os.rename(ff, ff + '-')
    hdl_log.close()
    conn.close()


def findBestHits(dbPath, paramDic, marker="*"):
    print(f"\n{bcolors.CVIOLET2}----Marking the best hits----{bcolors.CEND}")
    conn = lit.connect(dbPath)
    cur = conn.cursor()
    print('\tRemoving tags...')
    conn.execute("update tbl_blastxDiamond set tag=null;")
    conn.commit()

    print(f'\tFinding different reference database')
    refDbs = [rr[0] for rr in cur.execute("select distinct dbname from tbl_blastxDiamond;")]
    print(f"\t\tDetected {len(refDbs)} reference database: {bcolors.CYELLOW} {', '.join(refDbs)}{bcolors.CEND}")
    bestHitformual = ("max(qcoverage*identity)", "max(bitScore)")[paramDic["bestHit"] == 2]
    for dbname in refDbs:
        print(f'\n\t-Fetching hits in {dbname}', end='')
        cmd = f"select Id,orfAcc,{bestHitformual} from tbl_blastxDiamond where dbName='{dbname}' group by orfAcc;"
        ids = [0]
        rows = cur.execute(cmd).fetchall()
        cnt = 0
        for rr in rows:
            ids.append(rr[0])
            cur.execute("update tbl_blastxDiamond set tag=? where id=?",(marker,rr[0]))
            cnt+=1
        print(f"\t{cnt} rows will be updated.")
    conn.commit()
    conn.close()
    print(f"\tChanges were saved!")
    print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')


def exportResults(dbPath,outDir,identity=50,coverage=50,sep=";"):
    print(f"\n{bcolors.CVIOLET2}----Exporting results----{bcolors.CEND}")
    print(f"\t{bcolors.CYELLOW}Thresholds: identity>= {identity}% and coverage>= {coverage}%{bcolors.CEND}")

    conn = lit.connect(dbPath)
    cmd = f"select bx.Id as blastId,pr.id as orfID,pr.contigAcc as inputFile,pr.orfAcc,bx.refId,bx.identity,bx.qcoverage,bx.scoverage,bx.description,bx.length as [alignmentLength (aa)]" \
          f",bx.qlen as [queryLength (nt)],bx.slen as [subjectLength (aa)],bx.mismatch,bx.gap,bx.qstart,bx.qend,bx.sstart,bx.send,bx.eValue,bx.bitScore,bx.dbName " \
          f" from tbl_blastxDiamond bx, tbl_prodigal as pr where bx.orfAcc = pr.id and bx.tag is not null " \
         f" and identity>={identity} and qcoverage>={coverage} order by inputFile;"

    df = pd.read_sql_query(cmd,conn,index_col="blastId")
    outF = os.path.join(outDir,f'diamond_results_{identity}_{coverage}.csv')
    df.to_csv(outF,sep=sep)
    conn.close()
    print(f'\t{bcolors.CGREEN}Check the results in: {bcolors.CURL}{outF}{bcolors.CEND}')
    print(f'{bcolors.CGREEN}\nDone!{bcolors.CEND}')
