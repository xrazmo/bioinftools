import os
import sys, getopt
from glob import glob

from lib import Prodigal as prodig, Diamond as dmnd, Utility as ut
from datetime import datetime as dt
from lib.Utility import bcolors


def main(argv):
    if len(argv) < 1:
        printHelp()
        return
    command = argv[0]
    if command not in {'run', 'resume', 'export'}:
        print(f"{bcolors.CRED2}Error: Could not find the input command.{bcolors.CEND}")
        printHelp()
    try:
        opts, args = getopt.getopt(argv[1:], "hi:t:o:")
    except getopt.GetoptError:
        printHelp()
        sys.exit(2)
    paramDic = {}
    for opt, arg in opts:
        if opt == '-h':
            print('annotator.py -i <config.yml>')
            sys.exit()
        elif opt in ("-i",):  # check for short and long arguments
            paramDic['i'] = arg
        elif opt in ("-t"):
            paramDic['t'] = arg
        elif opt in ("-o"):
            paramDic['o'] = arg

    failed, msg = False, ''
    if command == 'run':
        if "i" in paramDic:
            runPipline(paramDic, 0)
        else:
            failed, msg = True, "could not find the input config file."
    elif command == 'resume':
        if "i" in paramDic:
            if "t" in paramDic:
                status = ut.readStatus(paramDic["t"])
                runPipline(paramDic, status)
            else:
                failed, msg = True, "Please specify the tmp directory (-t)."
        else:
            failed, msg = True, "Please specify the config file (-i)."
    else:  # command=='export'
        if "i" in paramDic:
            if "t" in paramDic:
               runPipline(paramDic, 8)
            else:
                failed, msg = True, "could not access reference database.Please specify the tmp directory. (-t)."
        else:
            failed, msg = True, "Please specify the config file (-i)."

    if failed:
        print(f'{bcolors.CRED2}Error:{msg}.{bcolors.CEND}')
        printHelp()


def runPipline(paramDic, status):
    confdic = ut.readConfFile(paramDic["i"], show=True)

    if status!=0:
        print(f"{bcolors.CVIOLET2}----Detecting previous steps----{bcolors.CEND}")
    if status < 1:
        print('Creating tmp directory:')
        tmpDir = os.path.join(confdic['output_dir'], f"_tmp_annot_{dt.now().strftime('%Y_%m_%d_%H-%M')}")
        ut.makeDirectory(tmpDir)
        print('Creating reference database...')
        refDB = ut.createRefDB(tmpDir)
        print(f'-Reference database: {bcolors.CGREEN}{bcolors.CURL}{refDB}{bcolors.CEND}{bcolors.CEND}')
        ut.writeStatus(tmpDir,1)
    else:
        tmpDir = paramDic["t"]
        refDB = os.path.join(tmpDir, 'refDB.db')
        print(f'-Tmp directory: {bcolors.CGREEN}{bcolors.CURL}{tmpDir}{bcolors.CEND}{bcolors.CEND}')
        print(f'-Reference database: {bcolors.CGREEN}{bcolors.CURL}{refDB}{bcolors.CEND}{bcolors.CEND}')

    prodigalOptDic = confdic['prodigal']
    opt = prodig.ProdigalOption(confdic['input_dirs'],
                                tmpDir, extension=f"*.{prodigalOptDic['extension']}",
                                instanceNum=prodigalOptDic["instances"],
                                out_nucl=True,
                                prodigalcmd=prodigalOptDic["commandPath"],
                                dbPath=refDB,
                                saveinDB=True)
    if status < 2:
        prodigalDir = prodig.runProdigal(opt)
        ut.writeStatus(tmpDir,2)
    else:
        prodigalDir = glob(os.path.join(tmpDir, "prodigalResult*"))[0]
        print(f'-Prodigal directory: {bcolors.CGREEN}{bcolors.CURL}{prodigalDir}{bcolors.CEND}{bcolors.CEND}')

    if status < 3:
        if (not prodig.saveResult(opt.dbPath, prodigalDir, (opt.saveGene and opt.out_nucl),
                                  (opt.saveProtein and opt.out_prot))):
            return
        ut.writeStatus(tmpDir,3)

    if status < 4:
        print('Creating ORF directory:')
        orfSeqDir = os.path.join(prodigalDir, 'ren_orfdir')
        ut.makeDirectory(orfSeqDir)
        prodig.renameProdigalGenes(refDB, prodigalDir, orfSeqDir, threads=prodigalOptDic["instances"])
        ut.writeStatus(tmpDir,4)
    else:
        orfSeqDir = glob(os.path.join(prodigalDir, 'ren_orfdir'))[0]
        print(f'-ORFs directory: {bcolors.CGREEN}{bcolors.CURL}{orfSeqDir}{bcolors.CEND}{bcolors.CEND}')

    diamondOptDic = confdic['diamond']
    dmndOpt = {}
    dmndOpt["curDir"] = tmpDir
    dmndOpt["commandPath"] = diamondOptDic["commandPath"]
    dmndOpt["exit"] = 0
    dmndOpt["task"] = 'blastx'
    option = diamondOptDic["options"]
    dmndOpt["outCols"] = "-f 6 qseqid sseqid pident mismatch gaps evalue bitscore length " \
                         "qlen slen qstart qend sstart send stitle"
    dmndOpt["dbs"] = []
    for db in diamondOptDic["dmnd_dbs"]:
        dbname = os.path.basename(db).split('.dmnd')[0]
        dmndOpt["dbs"].append([dbname, db, option])
    dmndOpt["bestHit"] = diamondOptDic["options"]
    dmndOpt["inputDir"] = [orfSeqDir]
    dmndOpt["valid_extensions"] = ["fa", "fasta", "fna"]
    dmndOpt["batchSize"] = 20
    dmndOpt["instance"] = diamondOptDic["threads"]
    dmndOpt["threads"] = diamondOptDic["instances"]

    if status < 5:
        b6Directory = dmnd.runDiamond_parallel(dmndOpt)
        ut.writeStatus(tmpDir,5)
    else:
        b6Directory = glob(os.path.join(tmpDir, "diamond*"))[0]
        print(f'-Diamond directory: {bcolors.CGREEN}{bcolors.CURL}{b6Directory}{bcolors.CEND}{bcolors.CEND}')

    if status < 6:
        dmnd.saveResultsDirectory(refDB, b6Directory, ["*.b6"], dmndOpt)
        ut.writeStatus(tmpDir,6)
    if status < 7:
        dmnd.findBestHits(refDB, dmndOpt)
        ut.writeStatus(tmpDir,7)
    if status < 8:
        dmnd.exportResults(refDB, confdic['output_dir'],
                           identity=diamondOptDic["min_identity"]
                           , coverage=diamondOptDic["min_qCoverage"])
        ut.writeStatus(tmpDir,8)
    else:
        print(f'\b{bcolors.CBOLD}{bcolors.CGREEN}All has been done!{bcolors.CEND}{bcolors.CEND}'
              f'\nTry to export results, or run the pipeline again.')


def printHelp():
    print(f"\n{bcolors.CVIOLET2}-----HELP------{bcolors.CEND}")
    print(
        f'{bcolors.CWHITEBG2}{bcolors.CBLACK}python annotator.py [commands:run/resume/export] -i <YAML config file> [-t <tmp directory>]{bcolors.CEND}\n')
    print(f'\tpython annotator.py {bcolors.CBEIGE}run -i <YAML config file>]{bcolors.CEND}\n'
          f'\t\tRunning the pipeline from the beginning by making a tmp directory, running Prodigal and then Diamond.\n')
    print(f'\tpython annotator.py {bcolors.CBEIGE}resume -i <YAML config file>] -t <tmp directory>{bcolors.CEND}\n'
          f'\t\tSearching through the input tmp directory for the intermediate results and then it tries to finish the pipeline from there.\n')
    print(f'\tpython annotator.py {bcolors.CBEIGE}export -i <YAML config file>] -t <tmp directory>{bcolors.CEND}\n'
          f'\t\tSearching through the input tmp directory to find the reference database and then export the results.\n')
    print(f"{bcolors.CVIOLET2}---------------{bcolors.CEND}")
if __name__ == "__main__":
    main(sys.argv[1:])
