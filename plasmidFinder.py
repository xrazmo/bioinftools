import os
import sys, getopt
from glob import glob
from lib import PlasmidFinder as plFin, Utility as ut
from datetime import datetime as dt
from lib.Utility import bcolors


def main(argv):
    if len(argv) < 1:
        printHelp()
        return
    command = argv[0]
    if command not in {'run', 'resume', 'export'}:
        print(f"{bcolors.CRED2}Error: Could not find the input command.{bcolors.CEND}")
        printHelp()
    try:
        opts, args = getopt.getopt(argv[1:], "hi:t:o:")
    except getopt.GetoptError:
        printHelp()
        sys.exit(2)
    paramDic = {}
    for opt, arg in opts:
        if opt == '-h':
            print('annotator.py -i <config.yml>')
            sys.exit()
        elif opt in ("-i",):  # check for short and long arguments
            paramDic['i'] = arg
        elif opt in ("-t"):
            paramDic['t'] = arg
        elif opt in ("-o"):
            paramDic['o'] = arg

    failed, msg = False, ''
    if command == 'run':
        if "i" in paramDic:
            runPipline(paramDic, 0)
        else:
            failed, msg = True, "could not find the input config file."
    elif command == 'resume':
        if "i" in paramDic:
            if "t" in paramDic:
                status = ut.readStatus(paramDic["t"])
                runPipline(paramDic, status)
            else:
                failed, msg = True, "Please specify the tmp directory (-t)."
        else:
            failed, msg = True, "Please specify the config file (-i)."
    else:  # command=='export'
        if "i" in paramDic:
            if "t" in paramDic:
                runPipline(paramDic, 8)
            else:
                failed, msg = True, "could not access reference database.Please specify the tmp directory. (-t)."
        else:
            failed, msg = True, "Please specify the config file (-i)."

    if failed:
        print(f'{bcolors.CRED2}Error:{msg}.{bcolors.CEND}')
        printHelp()


def runPipline(paramDic, status):
    confdic = ut.readConfFile(paramDic["i"], show=True)
    plfinConf = confdic["plasmid_finder"]
    if status != 0:
        print(f"{bcolors.CVIOLET2}----Detecting previous steps----{bcolors.CEND}")
    if status < 1:
        print('Creating tmp directory:')
        tmpDir = os.path.join(confdic['output_dir'], f"_tmp_plfin_{dt.now().strftime('%Y_%m_%d_%H-%M')}")
        ut.makeDirectory(tmpDir)
        print('Creating reference database...')
        refDB = ut.createRefDB(tmpDir)
        print(f'-Reference database: {bcolors.CGREEN}{bcolors.CURL}{refDB}{bcolors.CEND}{bcolors.CEND}')
        ut.writeStatus(tmpDir, 1)
    else:
        tmpDir = paramDic["t"]
        refDB = os.path.join(tmpDir, 'refDB.db')
        print(f'-Tmp directory: {bcolors.CGREEN}{bcolors.CURL}{tmpDir}{bcolors.CEND}{bcolors.CEND}')
        print(f'-Reference database: {bcolors.CGREEN}{bcolors.CURL}{refDB}{bcolors.CEND}{bcolors.CEND}')

    if status < 2:
        if plfinConf["download"] == 1:
            if not plFin.downloadDB(tmpDir): return
            faFile = os.path.join(tmpDir, 'plasmidfinder_db/enterobacteriaceae.fsa')
        else:
            faFile = plfinConf["databasePath"]
        plfin_db = plFin.createBlastDB(faFile, os.path.join(tmpDir, "plasmidfinder_db"))
        ut.writeStatus(tmpDir, 2)
    else:
        plasmidFinderDir = os.path.join(tmpDir, "plasmidfinder_db")
        plfin_db = os.path.join(tmpDir, "plasmidfinder_db/plfi_db")
        faFile = os.path.join(tmpDir, 'plasmidfinder_db/enterobacteriaceae.fsa')
        if not os.path.exists(plasmidFinderDir):
            print(f"\t{bcolors.CRED2}Error: could not locate plasmidFinder directory in here:"
                  f" {bcolors.CURL}{tmpDir}{bcolors.CEND}.{bcolors.CEND}")
            return
        if not os.path.exists(os.path.join(plfin_db, ".nsq")):
            print(f"\t{bcolors.CRED2}Error: could not locate plasmidFinder NCBI database in here:"
                  f" {bcolors.CURL}{plasmidFinderDir}{bcolors.CEND}.{bcolors.CEND}")
            return
        print(f'-PlasmidFinder directory: {bcolors.CGREEN}{bcolors.CURL}{plasmidFinderDir}{bcolors.CEND}{bcolors.CEND}')
        print(f'-PlasmidFinder database: {bcolors.CGREEN}{bcolors.CURL}{plfin_db}{bcolors.CEND}{bcolors.CEND}')

    if status < 3:
        b6Directory = plFin.ncbiBlastn_parallell(confdic["input_dirs"], tmpDir, plfin_db,
                                                 extension=plfinConf["extension"], threadNum=plfinConf["threads"]
                                                 , instanceNum=plfinConf["instances"],
                                                 stringOption=plfinConf["options"])
        ut.writeStatus(tmpDir, 3)
    else:
        b6Directory = glob(os.path.join(tmpDir, "blastn*"))[0]
        if not os.path.exists(b6Directory):
            print(f"\t{bcolors.CRED2}Error: could not locate blastn directory in here:"
                  f" {bcolors.CURL}{tmpDir}{bcolors.CEND}.{bcolors.CEND}")
            return
        print(f'-Blastn directory: {bcolors.CGREEN}{bcolors.CURL}{b6Directory}{bcolors.CEND}{bcolors.CEND}')

    if status < 4:
        plFin.saveBlastn(refDB, b6Directory)
        ut.writeStatus(tmpDir, 4)

    if status < 5:
        plFin.updateCov(refDB, faFile)
        ut.writeStatus(tmpDir, 5)
    if status < 6:
        plFin.exportResults(refDB, confdic['output_dir'],
                            identity=plfinConf["min_identity"]
                            , coverage=plfinConf["min_qCoverage"])
        ut.writeStatus(tmpDir, 6)
    else:
        print(f'\b{bcolors.CBOLD}{bcolors.CGREEN}All has been done!{bcolors.CEND}{bcolors.CEND}'
              f'\nTry to export results, or run the pipeline again.')


def printHelp():
    print(f"\n{bcolors.CVIOLET2}-----HELP------{bcolors.CEND}\n")
    print(
        f'{bcolors.CWHITEBG2}{bcolors.CBLACK}python plasmidFinder.py [commands:run/resume/export] -i <YAML config file> [-t <tmp directory>]{bcolors.CEND}\n')
    print(f'\tpython plasmidFinder.py {bcolors.CBEIGE}run -i <YAML config file>]{bcolors.CEND}\n'
          f'\t\tRunning the pipeline from the beginning by making a tmp directory, running Prodigal and then Diamond.\n')
    print(f'\tpython plasmidFinder.py {bcolors.CBEIGE}resume -i <YAML config file>] -t <tmp directory>{bcolors.CEND}\n'
          f'\t\tSearching through the input tmp directory for the intermediate results and then it tries to finish the pipeline from there.\n')
    print(f'\tpython plasmidFinder.py {bcolors.CBEIGE}export -i <YAML config file>] -t <tmp directory>{bcolors.CEND}\n'
          f'\t\tSearching through the input tmp directory to find the reference database and then export the results.\n')
    print(f"{bcolors.CVIOLET2}---------------{bcolors.CEND}")


if __name__ == "__main__":
    main(sys.argv[1:])
